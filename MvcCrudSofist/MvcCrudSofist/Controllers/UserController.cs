﻿using MvcCrudSofist.Models;
using MvcCrudSofist.Service;
using System; 
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcCrudSofist.Controllers
{
    public class UserController : Controller
    {
        UserDAL userDAL = new UserDAL();
        // GET: User
        public ActionResult List()
        {
            var data = userDAL.GetUsers();           
            return View(data);
            
        }


        public ActionResult Create()
        {
            
            return View();
        }


     
        [HttpPost]
        public ActionResult Create(UserModel user, string Service1,string Service2,string Service3, string Active, string Inactive)
        {

            if (Service1 == "true" && Service2 == "true" && Service3 == "true")
            {
                user.Services = "Service 1" + " " + "Service 2" + " " + "Service 3";
            }

            else if (Service1 == "true" && Service2 == "true")
            {
                user.Services = "Service 1" + " " + "Service 2";
            }

            else if (Service2 == "true" && Service3 == "true")
            {
                user.Services = "Service 2" + " " + "Service 3";
            }

            else if (Service1 == "true" && Service3 == "true")
            {
                user.Services = "Service 1" +" "+ "Service 3";
            }
            else if (Service1 == "true")
            {
                user.Services = "Service 1";
            }

            else if (Service2 == "true")
            {
                user.Services = "Service 2";
            }
            else if (Service3 == "true")
            {
                user.Services = "Service 3";
            }

            if (Active == null)
            {
                user.Status= "Active";
            }
            else if ( Inactive == null )
            {
                user.Status = "Inctive";
            }


            if (userDAL.InsertUser(user)) 
            {
                TempData["InsertMsg"] = "Vendor Details Saved Sucessfully...";
                return RedirectToAction("List");
            }
            else
            {
                TempData["InsertErrorMsg"] = "Data Not Saved";
            }
            return View();
        }

        public ActionResult Details(int? id)
        {
            var data = userDAL.GetUsers().Find(a => a.SrNo == id);
            return View(data);
        }


        public ActionResult Edit (int id)
        {
            var data = userDAL.GetUsers().Find(a => a.SrNo == id);
            return View(data);
        }


        [HttpPost]
        public ActionResult Edit(UserModel user, string Service1, string Service2, string Service3, string Active, string Inactive)
        {

            if (Service1 == "true" && Service2 == "true" && Service3 == "true")
            {
                user.Services = "Service 1" + " " + "Service 2" + " " + "Service 3";
            }

            else if (Service1 == "true" && Service2 == "true")
            {
                user.Services = "Service 1" + " " + "Service 2";
            }

            else if (Service2 == "true" && Service3 == "true")
            {
                user.Services = "Service 2" + " " + "Service 3";
            }

            else if (Service1 == "true" && Service3 == "true")
            {
                user.Services = "Service 1" + " " + "Service 3";
            }
            else if (Service1 == "true")
            {
                user.Services = "Service 1";
            }

            else if (Service2 == "true")
            {
                user.Services = "Service 2";
            }
            else if (Service3 == "true")
            {
                user.Services = "Service 3";
            }

            if (Active == null)
            {
                user.Status = "Active";
            }
            else if (Inactive == null)
            {
                user.Status = "Inactive";
            }


            if (userDAL.UpdateUser(user))
            {
                TempData["UpdateMsg"] = "Vendor Details UPDATED Sucessfully...";
                return RedirectToAction("List");
            }
            else
            {
                TempData["UpdateErrorMsg"] = "Data Not Saved";
            }
            return View();
        }


       // [HttpPost]
        public ActionResult Delete(int id)
        {
            int r = userDAL.DeleteUser(id);
            if (r>0)
            {
                TempData["DeleteMsg"] = "Vendor DELETED Sucessfully...";
                return RedirectToAction("List");
            }
            else
            {
                TempData["DeleteErrorMsg"] = "Data Not Deleted";
            }
            return View();
        }
       


    }
}