﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcCrudSofist.Models
{
    public class UserModel
    {
        
        public int SrNo { get; set; }
        //[Required(ErrorMessage ="vendor is required")]
        //[Display(Vendor = "Id")]

        [DisplayName("Vendor Name")]
        public string Vendor { get; set; }

        public DateTime Date { get; set; }
        public string Services { get; set; }
        public string Status { get; set; }

    }
}