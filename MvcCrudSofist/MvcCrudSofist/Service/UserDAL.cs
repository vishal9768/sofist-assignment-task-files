﻿using MvcCrudSofist.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;

namespace MvcCrudSofist.Service
{
    public class UserDAL
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cs"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter sda;
        DataTable dt;

        public List<UserModel> GetUsers()
        {
            cmd=new SqlCommand("sp_select", con);
            cmd.CommandType = CommandType.StoredProcedure;
            sda= new SqlDataAdapter(cmd);
            dt= new DataTable();
            sda.Fill(dt);
            List<UserModel> list = new List<UserModel>();
            
            foreach(DataRow dr in dt.Rows)
            {
                list.Add(new UserModel
                {
                    SrNo =Convert.ToInt32(dr["SrNo"]),
                    Vendor = dr["Vendor"].ToString(),
                    Date = Convert.ToDateTime(dr["Date"]),
                    Services = dr["Services"].ToString(),
                    Status = dr["Status"].ToString(),
                });
            }
            return list;


        }

        public bool InsertUser(UserModel user)
        {
            try
            {
                cmd = new SqlCommand("sp_insert", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@vendor", user.Vendor);
                cmd.Parameters.AddWithValue("@date", user.Date);
                cmd.Parameters.AddWithValue("@services", user.Services);
                cmd.Parameters.AddWithValue("@status", user.Status);
                con.Open();
                int r = cmd.ExecuteNonQuery();
                if (r > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool UpdateUser(UserModel user)
        {
            try
            {
                cmd = new SqlCommand("sp_update", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@vendor", user.Vendor);
                cmd.Parameters.AddWithValue("@date", user.Date);
                cmd.Parameters.AddWithValue("@services", user.Services);
                cmd.Parameters.AddWithValue("@status", user.Status);
                cmd.Parameters.AddWithValue("@srno", user.SrNo);
                con.Open();
                int r = cmd.ExecuteNonQuery();
                if (r > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int DeleteUser(int id)
        {
            try
            {
                cmd = new SqlCommand("sp_delete", con);
                cmd.CommandType = CommandType.StoredProcedure;
     
                cmd.Parameters.AddWithValue("@srno", id);
                con.Open();
                return cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {

                throw;
            }
            
        }




    }

}